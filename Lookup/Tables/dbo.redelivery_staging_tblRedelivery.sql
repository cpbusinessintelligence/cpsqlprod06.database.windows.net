SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redelivery_staging_tblRedelivery] (
		[RedeliveryID]            [int] NOT NULL,
		[RedeliveryType]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CardReferenceNumber]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[LableNumber]             [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[ConsignmentCode]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SenderName]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NumberOfItem]            [int] NULL,
		[State]                   [int] NOT NULL,
		[DestinationAddress]      [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DestinationSuburb]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[DestinationPostCode]     [int] NOT NULL,
		[SelectedETADate]         [date] NOT NULL,
		[Firstname]               [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Lastname]                [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[Email]                   [nvarchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[PhoneNumber]             [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SPInstruction]           [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_tblRedelivery]
		PRIMARY KEY
		CLUSTERED
		([RedeliveryID])
) TEXTIMAGE_ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [nc_idx_staging_tblRedelivery_ConsignmentCode]
	ON [dbo].[redelivery_staging_tblRedelivery] ([ConsignmentCode])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[redelivery_staging_tblRedelivery] SET (LOCK_ESCALATION = TABLE)
GO
