SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_updated_cdadditional] (
		[ca_consignment]     [int] NOT NULL,
		[ca_atl]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ca_dg]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK__edi_upda__64D01729BE1711C6]
		PRIMARY KEY
		CLUSTERED
		([ca_consignment])
)
GO
ALTER TABLE [dbo].[edi_updated_cdadditional]
	ADD
	CONSTRAINT [DF__edi_updat__ca_at__5CF6C6BC]
	DEFAULT ('N') FOR [ca_atl]
GO
ALTER TABLE [dbo].[edi_updated_cdadditional]
	ADD
	CONSTRAINT [DF__edi_updat__ca_dg__5DEAEAF5]
	DEFAULT ('N') FOR [ca_dg]
GO
ALTER TABLE [dbo].[edi_updated_cdadditional] SET (LOCK_ESCALATION = TABLE)
GO
