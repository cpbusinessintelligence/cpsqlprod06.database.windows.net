SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redirection_edi_incremental_consignment] (
		[cd_id]                             [int] NOT NULL,
		[cd_account]                        [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_connote]                        [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                           [smalldatetime] NULL,
		[cd_consignment_date]               [smalldatetime] NULL,
		[cd_pickup_addr0]                   [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                   [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                   [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                   [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                  [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_state_name]              [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]                [int] NULL,
		[cd_pickup_contact]                 [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]           [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                 [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_state_name]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]              [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_country]               [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]         [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]           [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_items]                          [int] NULL,
		[cd_deadweight]                     [float] NULL,
		[cd_volume]                         [float] NULL,
		[cd_pricecode]                      [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                      [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_status_flag]           [varchar](2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_redirected_delivery_option]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_rescheduled_delivery_date]      [datetime] NULL,
		CONSTRAINT [PK__redirect__D551B536181A2883]
		PRIMARY KEY
		CLUSTERED
		([cd_id])
)
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_ac__1CDC41A7]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_co__1DD065E0]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_da__1EC48A19]
	DEFAULT (NULL) FOR [cd_date]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_co__1FB8AE52]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__20ACD28B]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__21A0F6C4]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__22951AFD]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__23893F36]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__247D636F]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__257187A8]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__2665ABE1]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pi__2759D01A]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_sp__284DF453]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_it__2942188C]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_de__2A363CC5]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_vo__2B2A60FE]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_pr__2C1E8537]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment]
	ADD
	CONSTRAINT [DF__redirecti__cd_in__2D12A970]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[redirection_edi_incremental_consignment] SET (LOCK_ESCALATION = TABLE)
GO
