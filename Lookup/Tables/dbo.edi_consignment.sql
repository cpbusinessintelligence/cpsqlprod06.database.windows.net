SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_consignment] (
		[cd_id]                                       [int] NOT NULL,
		[cd_company_id]                               [int] NOT NULL,
		[cd_account]                                  [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_id]                                 [int] NULL,
		[cd_import_id]                                [int] NULL,
		[cd_ogm_id]                                   [int] NULL,
		[cd_manifest_id]                              [int] NULL,
		[cd_connote]                                  [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                                     [smalldatetime] NOT NULL,
		[cd_consignment_date]                         [smalldatetime] NULL,
		[cd_eta_date]                                 [smalldatetime] NULL,
		[cd_eta_earliest]                             [smalldatetime] NULL,
		[cd_customer_eta]                             [smalldatetime] NULL,
		[cd_pickup_addr0]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                             [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                            [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]                          [int] NULL,
		[cd_pickup_record_no]                         [int] NULL,
		[cd_pickup_confidence]                        [int] NULL,
		[cd_pickup_contact]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]                     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                           [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                           [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]                          [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]                        [int] NULL,
		[cd_delivery_record_no]                       [int] NULL,
		[cd_delivery_confidence]                      [int] NULL,
		[cd_delivery_contact]                         [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]                   [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]                     [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_stats_branch]                             [int] NULL,
		[cd_stats_depot]                              [int] NULL,
		[cd_pickup_branch]                            [int] NULL,
		[cd_pickup_pay_branch]                        [int] NULL,
		[cd_deliver_branch]                           [int] NULL,
		[cd_deliver_pay_branch]                       [int] NULL,
		[cd_special_driver]                           [int] NULL,
		[cd_pickup_revenue]                           [float] NULL,
		[cd_deliver_revenue]                          [float] NULL,
		[cd_pickup_billing]                           [float] NULL,
		[cd_deliver_billing]                          [float] NULL,
		[cd_pickup_charge]                            [float] NULL,
		[cd_pickup_charge_actual]                     [float] NULL,
		[cd_deliver_charge]                           [float] NULL,
		[cd_deliver_payment_actual]                   [float] NULL,
		[cd_pickup_payment]                           [float] NULL,
		[cd_pickup_payment_actual]                    [float] NULL,
		[cd_deliver_payment]                          [float] NULL,
		[cd_deliver_charge_actual]                    [float] NULL,
		[cd_special_payment]                          [float] NULL,
		[cd_insurance_billing]                        [float] NULL,
		[cd_items]                                    [int] NULL,
		[cd_coupons]                                  [int] NULL,
		[cd_references]                               [int] NULL,
		[cd_rating_id]                                [int] NULL,
		[cd_chargeunits]                              [int] NULL,
		[cd_deadweight]                               [float] NULL,
		[cd_dimension0]                               [float] NULL,
		[cd_dimension1]                               [float] NULL,
		[cd_dimension2]                               [float] NULL,
		[cd_volume]                                   [float] NULL,
		[cd_volume_automatic]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_import_deadweight]                        [float] NULL,
		[cd_import_volume]                            [float] NULL,
		[cd_measured_deadweight]                      [float] NULL,
		[cd_measured_volume]                          [float] NULL,
		[cd_billing_id]                               [int] NULL,
		[cd_billing_date]                             [smalldatetime] NULL,
		[cd_export_id]                                [int] NULL,
		[cd_export2_id]                               [int] NULL,
		[cd_pickup_pay_date]                          [smalldatetime] NULL,
		[cd_delivery_pay_date]                        [smalldatetime] NULL,
		[cd_transfer_pay_date]                        [smalldatetime] NULL,
		[cd_activity_stamp]                           [datetime] NULL,
		[cd_activity_driver]                          [int] NULL,
		[cd_pickup_stamp]                             [datetime] NULL,
		[cd_pickup_driver]                            [int] NULL,
		[cd_pickup_pay_driver]                        [int] NULL,
		[cd_pickup_count]                             [int] NULL,
		[cd_pickup_notified]                          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_accept_stamp]                             [datetime] NULL,
		[cd_accept_driver]                            [int] NULL,
		[cd_accept_count]                             [int] NULL,
		[cd_accept_notified]                          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_notified]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_stamp]                            [datetime] NULL,
		[cd_indepot_driver]                           [int] NULL,
		[cd_indepot_count]                            [int] NULL,
		[cd_transfer_notified]                        [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_failed_stamp]                             [datetime] NULL,
		[cd_failed_driver]                            [int] NULL,
		[cd_deliver_stamp]                            [datetime] NULL,
		[cd_deliver_driver]                           [int] NULL,
		[cd_deliver_pay_driver]                       [int] NULL,
		[cd_deliver_count]                            [int] NULL,
		[cd_deliver_pod]                              [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_notified]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_pay_notified]                      [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_pay_notified]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_printed]                                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_returns]                                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release]                                  [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release_stamp]                            [datetime] NULL,
		[cd_pricecode]                                [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_agent]                             [int] NULL,
		[cd_delivery_agent]                           [int] NULL,
		[cd_agent_pod]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_desired]                        [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_name]                           [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_stamp]                          [datetime] NULL,
		[cd_agent_pod_entry]                          [datetime] NULL,
		[cd_completed]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled]                                [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled_stamp]                          [datetime] NULL,
		[cd_cancelled_by]                             [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_test]                                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_dirty]                                    [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_transfer_stamp]                           [datetime] NULL,
		[cd_transfer_driver]                          [int] NULL,
		[cd_transfer_count]                           [int] NULL,
		[cd_transfer_to]                              [int] NULL,
		[cd_toagent_notified]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_toagent_stamp]                            [datetime] NULL,
		[cd_toagent_driver]                           [int] NULL,
		[cd_toagent_count]                            [int] NULL,
		[cd_toagent_name]                             [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_status]                              [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_notified]                            [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_info]                                [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_driver]                              [int] NULL,
		[cd_last_stamp]                               [datetime] NULL,
		[cd_last_count]                               [int] NULL,
		[cd_accept_driver_branch]                     [int] NULL,
		[cd_activity_driver_branch]                   [int] NULL,
		[cd_deliver_driver_branch]                    [int] NULL,
		[cd_deliver_pay_driver_branch]                [int] NULL,
		[cd_failed_driver_branch]                     [int] NULL,
		[cd_indepot_driver_branch]                    [int] NULL,
		[cd_last_driver_branch]                       [int] NULL,
		[cd_pickup_driver_branch]                     [int] NULL,
		[cd_pickup_pay_driver_branch]                 [int] NULL,
		[cd_special_driver_branch]                    [int] NULL,
		[cd_toagent_driver_branch]                    [int] NULL,
		[cd_transfer_driver_branch]                   [int] NULL,
		[cd_pickup_state_name]                        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_state_name]                      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr0]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr1]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr2]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_addr3]                       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_email]                       [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_suburb]                      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_state_name]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_postcode]                    [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_country]                     [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_contact]                     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_new_delivery_contact_phone]               [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_rescheduled_delivery_date]                [datetime] NULL,
		[cd_lookup_created_stamp]                     [datetime] NOT NULL,
		[cd_lookup_updated_stamp]                     [datetime] NOT NULL,
		[cd_locked_for_editing_after_redirection]     [bit] NOT NULL,
		[cd_locked_for_editing_after_redelivery]      [bit] NOT NULL,
		CONSTRAINT [PK_edi_consignment]
		PRIMARY KEY
		CLUSTERED
		([cd_id], [cd_date])
	WITH (STATISTICS_NORECOMPUTE = ON)
)
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__00DF2177]
	DEFAULT ('0') FOR [cd_accept_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__01D345B0]
	DEFAULT ('N') FOR [cd_accept_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__02C769E9]
	DEFAULT ('N') FOR [cd_indepot_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__03BB8E22]
	DEFAULT (NULL) FOR [cd_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__04AFB25B]
	DEFAULT ('0') FOR [cd_indepot_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__05A3D694]
	DEFAULT ('0') FOR [cd_indepot_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__0697FACD]
	DEFAULT ('N') FOR [cd_transfer_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_fa__078C1F06]
	DEFAULT (NULL) FOR [cd_failed_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_fa__0880433F]
	DEFAULT ('0') FOR [cd_failed_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__09746778]
	DEFAULT (NULL) FOR [cd_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__0A688BB1]
	DEFAULT ('0') FOR [cd_deliver_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__0B5CAFEA]
	DEFAULT ('0') FOR [cd_deliver_pay_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_lo__0B7CAB7B]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [cd_lookup_created_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__0C50D423]
	DEFAULT ('0') FOR [cd_deliver_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_lo__0C70CFB4]
	DEFAULT (CONVERT([datetime],((getutcdate() AT TIME ZONE 'UTC') AT TIME ZONE 'AUS Eastern Standard Time'))) FOR [cd_lookup_updated_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__0D44F85C]
	DEFAULT ('') FOR [cd_deliver_pod]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__0E391C95]
	DEFAULT ('N') FOR [cd_deliver_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__0F2D40CE]
	DEFAULT ('N') FOR [cd_pickup_pay_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__10216507]
	DEFAULT ('N') FOR [cd_deliver_pay_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pr__11158940]
	DEFAULT ('Y') FOR [cd_printed]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__1209AD79]
	DEFAULT ('N') FOR [cd_returns]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__12FDD1B2]
	DEFAULT ('N') FOR [cd_release]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__13F1F5EB]
	DEFAULT (NULL) FOR [cd_release_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pr__14E61A24]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__15DA3E5D]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__16CE6296]
	DEFAULT ('0') FOR [cd_pickup_agent]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__17C286CF]
	DEFAULT ('0') FOR [cd_delivery_agent]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__18B6AB08]
	DEFAULT ('N') FOR [cd_agent_pod]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__19AACF41]
	DEFAULT ('N') FOR [cd_agent_pod_desired]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__1A9EF37A]
	DEFAULT ('') FOR [cd_agent_pod_name]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__1B9317B3]
	DEFAULT (NULL) FOR [cd_agent_pod_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__1C873BEC]
	DEFAULT (NULL) FOR [cd_agent_pod_entry]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__1D7B6025]
	DEFAULT ('N') FOR [cd_completed]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ca__1E6F845E]
	DEFAULT ('N') FOR [cd_cancelled]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ca__1F63A897]
	DEFAULT (NULL) FOR [cd_cancelled_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ca__2057CCD0]
	DEFAULT ('') FOR [cd_cancelled_by]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_te__214BF109]
	DEFAULT ('N') FOR [cd_test]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__22401542]
	DEFAULT ('Y') FOR [cd_dirty]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__2334397B]
	DEFAULT (NULL) FOR [cd_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__24285DB4]
	DEFAULT ('0') FOR [cd_transfer_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__251C81ED]
	DEFAULT ('0') FOR [cd_transfer_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__2610A626]
	DEFAULT ('0') FOR [cd_transfer_to]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__2704CA5F]
	DEFAULT ('N') FOR [cd_toagent_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__27F8EE98]
	DEFAULT (NULL) FOR [cd_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__28ED12D1]
	DEFAULT ('0') FOR [cd_toagent_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__29E1370A]
	DEFAULT ('0') FOR [cd_toagent_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__2AD55B43]
	DEFAULT ('') FOR [cd_toagent_name]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__2BC97F7C]
	DEFAULT ('') FOR [cd_last_status]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__2CBDA3B5]
	DEFAULT ('N') FOR [cd_last_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__2DB1C7EE]
	DEFAULT (NULL) FOR [cd_last_info]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__2EA5EC27]
	DEFAULT ('0') FOR [cd_last_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__2EDAF651]
	DEFAULT ('0') FOR [cd_company_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__2F9A1060]
	DEFAULT (NULL) FOR [cd_last_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__2FCF1A8A]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__308E3499]
	DEFAULT ('0') FOR [cd_last_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ag__30C33EC3]
	DEFAULT (NULL) FOR [cd_agent_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__318258D2]
	DEFAULT (NULL) FOR [cd_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_im__31B762FC]
	DEFAULT (NULL) FOR [cd_import_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__32767D0B]
	DEFAULT (NULL) FOR [cd_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_og__32AB8735]
	DEFAULT ('0') FOR [cd_ogm_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__336AA144]
	DEFAULT (NULL) FOR [cd_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ma__339FAB6E]
	DEFAULT ('0') FOR [cd_manifest_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__345EC57D]
	DEFAULT (NULL) FOR [cd_deliver_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__3493CFA7]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_fa__3552E9B6]
	DEFAULT (NULL) FOR [cd_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_da__3587F3E0]
	DEFAULT ('01-01-1900') FOR [cd_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__36470DEF]
	DEFAULT (NULL) FOR [cd_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__367C1819]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_la__373B3228]
	DEFAULT (NULL) FOR [cd_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_et__37703C52]
	DEFAULT (NULL) FOR [cd_eta_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__382F5661]
	DEFAULT (NULL) FOR [cd_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_et__3864608B]
	DEFAULT (NULL) FOR [cd_eta_earliest]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__39237A9A]
	DEFAULT (NULL) FOR [cd_pickup_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_cu__395884C4]
	DEFAULT (NULL) FOR [cd_customer_eta]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__3A179ED3]
	DEFAULT (NULL) FOR [cd_special_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3A4CA8FD]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_to__3B0BC30C]
	DEFAULT (NULL) FOR [cd_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3B40CD36]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__3BFFE745]
	DEFAULT (NULL) FOR [cd_transfer_driver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3C34F16F]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3D2915A8]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3E1D39E1]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_lo__3EDC53F0]
	DEFAULT ((0)) FOR [cd_locked_for_editing_after_redirection]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__3F115E1A]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_lo__3FD07829]
	DEFAULT ((0)) FOR [cd_locked_for_editing_after_redelivery]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__40058253]
	DEFAULT ('0') FOR [cd_pickup_record_no]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__40F9A68C]
	DEFAULT ('0') FOR [cd_pickup_confidence]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__41EDCAC5]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__42E1EEFE]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__43D61337]
	DEFAULT ('') FOR [cd_delivery_addr0]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__44CA3770]
	DEFAULT ('') FOR [cd_delivery_addr1]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__45BE5BA9]
	DEFAULT ('') FOR [cd_delivery_addr2]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__46B27FE2]
	DEFAULT ('') FOR [cd_delivery_addr3]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__47A6A41B]
	DEFAULT ('') FOR [cd_delivery_email]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__489AC854]
	DEFAULT ('') FOR [cd_delivery_suburb]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__498EEC8D]
	DEFAULT (NULL) FOR [cd_delivery_postcode]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__4A8310C6]
	DEFAULT ('0') FOR [cd_delivery_record_no]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__4B7734FF]
	DEFAULT ('0') FOR [cd_delivery_confidence]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__4C6B5938]
	DEFAULT ('') FOR [cd_delivery_contact]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__4D5F7D71]
	DEFAULT ('') FOR [cd_delivery_contact_phone]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__4E53A1AA]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_st__4F47C5E3]
	DEFAULT (NULL) FOR [cd_stats_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_st__503BEA1C]
	DEFAULT (NULL) FOR [cd_stats_depot]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__51300E55]
	DEFAULT (NULL) FOR [cd_pickup_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__5224328E]
	DEFAULT (NULL) FOR [cd_pickup_pay_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__531856C7]
	DEFAULT (NULL) FOR [cd_deliver_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__540C7B00]
	DEFAULT (NULL) FOR [cd_deliver_pay_branch]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__55009F39]
	DEFAULT ('0') FOR [cd_special_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__55F4C372]
	DEFAULT ('0') FOR [cd_pickup_revenue]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__56E8E7AB]
	DEFAULT ('0') FOR [cd_deliver_revenue]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__57DD0BE4]
	DEFAULT ('0') FOR [cd_pickup_billing]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__58D1301D]
	DEFAULT ('0') FOR [cd_deliver_billing]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__59C55456]
	DEFAULT ('0') FOR [cd_pickup_charge]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__5AB9788F]
	DEFAULT ('0') FOR [cd_pickup_charge_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__5BAD9CC8]
	DEFAULT ('0') FOR [cd_deliver_charge]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__5CA1C101]
	DEFAULT ('0') FOR [cd_deliver_payment_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__5D95E53A]
	DEFAULT ('0') FOR [cd_pickup_payment]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__5E8A0973]
	DEFAULT ('0') FOR [cd_pickup_payment_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__5F7E2DAC]
	DEFAULT ('0') FOR [cd_deliver_payment]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__607251E5]
	DEFAULT ('0') FOR [cd_deliver_charge_actual]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_sp__6166761E]
	DEFAULT ('0') FOR [cd_special_payment]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_in__625A9A57]
	DEFAULT ('0') FOR [cd_insurance_billing]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_it__634EBE90]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_co__6442E2C9]
	DEFAULT (NULL) FOR [cd_coupons]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_re__65370702]
	DEFAULT (NULL) FOR [cd_references]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ra__662B2B3B]
	DEFAULT (NULL) FOR [cd_rating_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ch__671F4F74]
	DEFAULT (NULL) FOR [cd_chargeunits]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__681373AD]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__690797E6]
	DEFAULT (NULL) FOR [cd_dimension0]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__69FBBC1F]
	DEFAULT (NULL) FOR [cd_dimension1]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_di__6AEFE058]
	DEFAULT (NULL) FOR [cd_dimension2]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_vo__6BE40491]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_vo__6CD828CA]
	DEFAULT ('N') FOR [cd_volume_automatic]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_im__6DCC4D03]
	DEFAULT (NULL) FOR [cd_import_deadweight]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_im__6EC0713C]
	DEFAULT (NULL) FOR [cd_import_volume]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_me__6FB49575]
	DEFAULT (NULL) FOR [cd_measured_deadweight]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_me__70A8B9AE]
	DEFAULT (NULL) FOR [cd_measured_volume]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_bi__719CDDE7]
	DEFAULT ('0') FOR [cd_billing_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_bi__72910220]
	DEFAULT (NULL) FOR [cd_billing_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ex__73852659]
	DEFAULT ('0') FOR [cd_export_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ex__74794A92]
	DEFAULT ('0') FOR [cd_export2_id]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__756D6ECB]
	DEFAULT (NULL) FOR [cd_pickup_pay_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_de__76619304]
	DEFAULT (NULL) FOR [cd_delivery_pay_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_tr__7755B73D]
	DEFAULT (NULL) FOR [cd_transfer_pay_date]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__7849DB76]
	DEFAULT (NULL) FOR [cd_activity_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__793DFFAF]
	DEFAULT ('0') FOR [cd_activity_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7A3223E8]
	DEFAULT (NULL) FOR [cd_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7B264821]
	DEFAULT ('0') FOR [cd_pickup_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7C1A6C5A]
	DEFAULT ('0') FOR [cd_pickup_pay_driver]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7D0E9093]
	DEFAULT ('0') FOR [cd_pickup_count]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_pi__7E02B4CC]
	DEFAULT ('N') FOR [cd_pickup_notified]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__7EF6D905]
	DEFAULT (NULL) FOR [cd_accept_stamp]
GO
ALTER TABLE [dbo].[edi_consignment]
	ADD
	CONSTRAINT [DF__edi_consi__cd_ac__7FEAFD3E]
	DEFAULT ('0') FOR [cd_accept_driver]
GO
CREATE NONCLUSTERED INDEX [nci_wi_edi_consignment_D3F9B2CAA774A7251856879BA592E6BA]
	ON [dbo].[edi_consignment] ([cd_locked_for_editing_after_redelivery], [cd_locked_for_editing_after_redirection])
	INCLUDE ([cd_connote])
	ON [partition_scheme_edi_consignment_by_month] ([cd_date])
GO
CREATE NONCLUSTERED INDEX [nc_idx_edi_consignment_cd_connote]
	ON [dbo].[edi_consignment] ([cd_connote])
	WITH ( STATISTICS_NORECOMPUTE = ON)
	ON [partition_scheme_edi_consignment_by_month] ([cd_date])
GO
CREATE NONCLUSTERED INDEX [nc_idx_edi_consignment_redelivery_redirected_locked_fields]
	ON [dbo].[edi_consignment] ([cd_locked_for_editing_after_redelivery], [cd_locked_for_editing_after_redirection])
	INCLUDE ([cd_account], [cd_connote], [cd_consignment_date], [cd_deadweight], [cd_delivery_addr0], [cd_delivery_addr1], [cd_delivery_addr2], [cd_delivery_addr3], [cd_delivery_contact], [cd_delivery_contact_phone], [cd_delivery_email], [cd_delivery_postcode], [cd_delivery_suburb], [cd_insurance], [cd_items], [cd_pickup_addr0], [cd_pickup_addr1], [cd_pickup_addr2], [cd_pickup_addr3], [cd_pickup_contact], [cd_pickup_contact_phone], [cd_pickup_postcode], [cd_pickup_suburb], [cd_pricecode], [cd_special_instructions], [cd_volume])
	WITH ( STATISTICS_NORECOMPUTE = ON)
	ON [partition_scheme_edi_consignment_by_month] ([cd_date])
GO
ALTER TABLE [dbo].[edi_consignment] SET (LOCK_ESCALATION = TABLE)
GO
