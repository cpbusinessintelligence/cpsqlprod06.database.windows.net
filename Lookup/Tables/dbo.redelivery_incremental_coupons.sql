SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redelivery_incremental_coupons] (
		[ConsignmentCode]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_redelivery_incremental_coupons]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentCode])
)
GO
ALTER TABLE [dbo].[redelivery_incremental_coupons] SET (LOCK_ESCALATION = TABLE)
GO
