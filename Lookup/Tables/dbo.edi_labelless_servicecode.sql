SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_labelless_servicecode] (
		[ID]              [int] IDENTITY(1, 1) NOT NULL,
		[ServiceCode]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[Description]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IsActive]        [int] NOT NULL,
		CONSTRAINT [PK_tbl_Labelless_ServiceCode]
		PRIMARY KEY
		NONCLUSTERED
		([ID])
)
GO
ALTER TABLE [dbo].[edi_labelless_servicecode] SET (LOCK_ESCALATION = TABLE)
GO
