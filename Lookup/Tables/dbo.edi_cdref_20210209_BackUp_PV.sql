SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_cdref_20210209_BackUp_PV] (
		[cr_id]              [int] NOT NULL,
		[cr_company_id]      [int] NOT NULL,
		[cr_consignment]     [int] NOT NULL,
		[cr_reference]       [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[edi_cdref_20210209_BackUp_PV] SET (LOCK_ESCALATION = TABLE)
GO
