SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_cdcoupon] (
		[cc_id]                         [int] NOT NULL,
		[cc_company_id]                 [int] NOT NULL,
		[cc_consignment]                [int] NOT NULL,
		[cc_coupon]                     [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_activity_stamp]             [datetime] NULL,
		[cc_pickup_stamp]               [datetime] NULL,
		[cc_accept_stamp]               [datetime] NULL,
		[cc_indepot_stamp]              [datetime] NULL,
		[cc_transfer_stamp]             [datetime] NULL,
		[cc_deliver_stamp]              [datetime] NULL,
		[cc_failed_stamp]               [datetime] NULL,
		[cc_activity_driver]            [int] NULL,
		[cc_pickup_driver]              [int] NULL,
		[cc_accept_driver]              [int] NULL,
		[cc_indepot_driver]             [int] NULL,
		[cc_transfer_driver]            [int] NULL,
		[cc_transfer_to]                [int] NULL,
		[cc_toagent_driver]             [int] NULL,
		[cc_toagent_stamp]              [datetime] NULL,
		[cc_toagent_name]               [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_deliver_driver]             [int] NULL,
		[cc_failed_driver]              [int] NULL,
		[cc_deliver_pod]                [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_failed]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_exception_stamp]            [datetime] NULL,
		[cc_exception_code]             [char](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_unit_type]                  [char](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_internal]                   [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_link_coupon]                [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_dirty]                      [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_last_status]                [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_last_driver]                [int] NULL,
		[cc_last_stamp]                 [datetime] NULL,
		[cc_last_info]                  [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_accept_driver_branch]       [int] NULL,
		[cc_activity_driver_branch]     [int] NULL,
		[cc_deliver_driver_branch]      [int] NULL,
		[cc_failed_driver_branch]       [int] NULL,
		[cc_indepot_driver_branch]      [int] NULL,
		[cc_last_driver_branch]         [int] NULL,
		[cc_pickup_driver_branch]       [int] NULL,
		[cc_toagent_driver_branch]      [int] NULL,
		[cc_tranfer_driver_branch]      [int] NULL,
		CONSTRAINT [PK_edi_cdcoupon]
		PRIMARY KEY
		CLUSTERED
		([cc_id])
	WITH (STATISTICS_NORECOMPUTE = ON)
)
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_co__03F0984C]
	DEFAULT ('0') FOR [cc_company_id]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_co__04E4BC85]
	DEFAULT ('0') FOR [cc_consignment]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_co__05D8E0BE]
	DEFAULT ('') FOR [cc_coupon]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__06CD04F7]
	DEFAULT (NULL) FOR [cc_activity_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_pi__07C12930]
	DEFAULT (NULL) FOR [cc_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__08B54D69]
	DEFAULT (NULL) FOR [cc_accept_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_in__09A971A2]
	DEFAULT (NULL) FOR [cc_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_tr__0A9D95DB]
	DEFAULT (NULL) FOR [cc_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_de__0B91BA14]
	DEFAULT (NULL) FOR [cc_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_fa__0C85DE4D]
	DEFAULT (NULL) FOR [cc_failed_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__0D7A0286]
	DEFAULT ('0') FOR [cc_activity_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_pi__0E6E26BF]
	DEFAULT ('0') FOR [cc_pickup_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__0F624AF8]
	DEFAULT ('0') FOR [cc_accept_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_in__10566F31]
	DEFAULT ('0') FOR [cc_indepot_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_tr__114A936A]
	DEFAULT ('0') FOR [cc_transfer_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_tr__123EB7A3]
	DEFAULT ('0') FOR [cc_transfer_to]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_to__1332DBDC]
	DEFAULT ('0') FOR [cc_toagent_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_to__14270015]
	DEFAULT (NULL) FOR [cc_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_to__151B244E]
	DEFAULT ('') FOR [cc_toagent_name]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_de__160F4887]
	DEFAULT ('0') FOR [cc_deliver_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_fa__17036CC0]
	DEFAULT ('0') FOR [cc_failed_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_de__17F790F9]
	DEFAULT ('') FOR [cc_deliver_pod]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_fa__18EBB532]
	DEFAULT ('N') FOR [cc_failed]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ex__19DFD96B]
	DEFAULT (NULL) FOR [cc_exception_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ex__1AD3FDA4]
	DEFAULT ('') FOR [cc_exception_code]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_un__1BC821DD]
	DEFAULT ('') FOR [cc_unit_type]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_in__1CBC4616]
	DEFAULT ('N') FOR [cc_internal]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_li__1DB06A4F]
	DEFAULT ('') FOR [cc_link_coupon]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_di__1EA48E88]
	DEFAULT ('Y') FOR [cc_dirty]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_la__1F98B2C1]
	DEFAULT ('') FOR [cc_last_status]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_la__208CD6FA]
	DEFAULT ('0') FOR [cc_last_driver]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_la__2180FB33]
	DEFAULT (NULL) FOR [cc_last_stamp]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_la__22751F6C]
	DEFAULT (NULL) FOR [cc_last_info]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__236943A5]
	DEFAULT (NULL) FOR [cc_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_ac__245D67DE]
	DEFAULT (NULL) FOR [cc_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_de__25518C17]
	DEFAULT (NULL) FOR [cc_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_fa__2645B050]
	DEFAULT (NULL) FOR [cc_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_in__2739D489]
	DEFAULT (NULL) FOR [cc_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_la__282DF8C2]
	DEFAULT (NULL) FOR [cc_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_pi__29221CFB]
	DEFAULT (NULL) FOR [cc_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_to__2A164134]
	DEFAULT (NULL) FOR [cc_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_cdcou__cc_tr__2B0A656D]
	DEFAULT (NULL) FOR [cc_tranfer_driver_branch]
GO
CREATE NONCLUSTERED INDEX [nc_idx_edi_cdcoupon_json]
	ON [dbo].[edi_cdcoupon] ([cc_consignment])
	INCLUDE ([cc_coupon], [cc_unit_type])
	WITH ( STATISTICS_NORECOMPUTE = ON)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[edi_cdcoupon] SET (LOCK_ESCALATION = TABLE)
GO
