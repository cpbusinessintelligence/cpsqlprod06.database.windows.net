SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_updated_cdcoupon] (
		[cc_id]                         [int] NOT NULL,
		[cc_company_id]                 [int] NOT NULL,
		[cc_consignment]                [int] NOT NULL,
		[cc_coupon]                     [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_activity_stamp]             [datetime] NULL,
		[cc_pickup_stamp]               [datetime] NULL,
		[cc_accept_stamp]               [datetime] NULL,
		[cc_indepot_stamp]              [datetime] NULL,
		[cc_transfer_stamp]             [datetime] NULL,
		[cc_deliver_stamp]              [datetime] NULL,
		[cc_failed_stamp]               [datetime] NULL,
		[cc_activity_driver]            [int] NULL,
		[cc_pickup_driver]              [int] NULL,
		[cc_accept_driver]              [int] NULL,
		[cc_indepot_driver]             [int] NULL,
		[cc_transfer_driver]            [int] NULL,
		[cc_transfer_to]                [int] NULL,
		[cc_toagent_driver]             [int] NULL,
		[cc_toagent_stamp]              [datetime] NULL,
		[cc_toagent_name]               [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_deliver_driver]             [int] NULL,
		[cc_failed_driver]              [int] NULL,
		[cc_deliver_pod]                [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_failed]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_exception_stamp]            [datetime] NULL,
		[cc_exception_code]             [char](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_unit_type]                  [char](8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_internal]                   [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_link_coupon]                [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_dirty]                      [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_last_status]                [char](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_last_driver]                [int] NULL,
		[cc_last_stamp]                 [datetime] NULL,
		[cc_last_info]                  [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cc_accept_driver_branch]       [int] NULL,
		[cc_activity_driver_branch]     [int] NULL,
		[cc_deliver_driver_branch]      [int] NULL,
		[cc_failed_driver_branch]       [int] NULL,
		[cc_indepot_driver_branch]      [int] NULL,
		[cc_last_driver_branch]         [int] NULL,
		[cc_pickup_driver_branch]       [int] NULL,
		[cc_toagent_driver_branch]      [int] NULL,
		[cc_tranfer_driver_branch]      [int] NULL,
		CONSTRAINT [PK__edi_upda__9F1E187B40FF1053]
		PRIMARY KEY
		CLUSTERED
		([cc_id])
)
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_de__004002F9]
	DEFAULT (NULL) FOR [cc_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_fa__01342732]
	DEFAULT (NULL) FOR [cc_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_in__02284B6B]
	DEFAULT (NULL) FOR [cc_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_la__031C6FA4]
	DEFAULT (NULL) FOR [cc_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_pi__041093DD]
	DEFAULT (NULL) FOR [cc_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_to__0504B816]
	DEFAULT (NULL) FOR [cc_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_tr__05F8DC4F]
	DEFAULT (NULL) FOR [cc_tranfer_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_co__5EDF0F2E]
	DEFAULT ('0') FOR [cc_company_id]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_co__5FD33367]
	DEFAULT ('0') FOR [cc_consignment]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_co__60C757A0]
	DEFAULT ('') FOR [cc_coupon]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__61BB7BD9]
	DEFAULT (NULL) FOR [cc_activity_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_pi__62AFA012]
	DEFAULT (NULL) FOR [cc_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__63A3C44B]
	DEFAULT (NULL) FOR [cc_accept_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_in__6497E884]
	DEFAULT (NULL) FOR [cc_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_tr__658C0CBD]
	DEFAULT (NULL) FOR [cc_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_de__668030F6]
	DEFAULT (NULL) FOR [cc_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_fa__6774552F]
	DEFAULT (NULL) FOR [cc_failed_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__68687968]
	DEFAULT ('0') FOR [cc_activity_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_pi__695C9DA1]
	DEFAULT ('0') FOR [cc_pickup_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__6A50C1DA]
	DEFAULT ('0') FOR [cc_accept_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_in__6B44E613]
	DEFAULT ('0') FOR [cc_indepot_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_tr__6C390A4C]
	DEFAULT ('0') FOR [cc_transfer_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_tr__6D2D2E85]
	DEFAULT ('0') FOR [cc_transfer_to]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_to__6E2152BE]
	DEFAULT ('0') FOR [cc_toagent_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_to__6F1576F7]
	DEFAULT (NULL) FOR [cc_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_to__70099B30]
	DEFAULT ('') FOR [cc_toagent_name]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_de__70FDBF69]
	DEFAULT ('0') FOR [cc_deliver_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_fa__71F1E3A2]
	DEFAULT ('0') FOR [cc_failed_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_de__72E607DB]
	DEFAULT ('') FOR [cc_deliver_pod]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_fa__73DA2C14]
	DEFAULT ('N') FOR [cc_failed]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ex__74CE504D]
	DEFAULT (NULL) FOR [cc_exception_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ex__75C27486]
	DEFAULT ('') FOR [cc_exception_code]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_un__76B698BF]
	DEFAULT ('') FOR [cc_unit_type]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_in__77AABCF8]
	DEFAULT ('N') FOR [cc_internal]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_li__789EE131]
	DEFAULT ('') FOR [cc_link_coupon]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_di__7993056A]
	DEFAULT ('Y') FOR [cc_dirty]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_la__7A8729A3]
	DEFAULT ('') FOR [cc_last_status]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_la__7B7B4DDC]
	DEFAULT ('0') FOR [cc_last_driver]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_la__7C6F7215]
	DEFAULT (NULL) FOR [cc_last_stamp]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_la__7D63964E]
	DEFAULT (NULL) FOR [cc_last_info]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__7E57BA87]
	DEFAULT (NULL) FOR [cc_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon]
	ADD
	CONSTRAINT [DF__edi_updat__cc_ac__7F4BDEC0]
	DEFAULT (NULL) FOR [cc_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_updated_cdcoupon] SET (LOCK_ESCALATION = TABLE)
GO
