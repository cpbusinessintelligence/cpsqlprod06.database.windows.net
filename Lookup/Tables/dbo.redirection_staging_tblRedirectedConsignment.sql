SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[redirection_staging_tblRedirectedConsignment] (
		[ConsignmentCode]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[SelectedDeliveryOption]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationFirstName]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationLastName]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationCompanyName]     [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationEmail]           [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationAddress1]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationAddress2]        [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationSuburb]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationStateName]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationStateID]         [int] NULL,
		[NewDestinationPostCode]        [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationPhone]           [nvarchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewDestinationCountry]         [nvarchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[NewETA]                        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[SpecialInstruction]            [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ATL]                           [bit] NULL,
		CONSTRAINT [PK__redirect__3A9C08E12A676E60]
		PRIMARY KEY
		CLUSTERED
		([ConsignmentCode])
)
GO
ALTER TABLE [dbo].[redirection_staging_tblRedirectedConsignment] SET (LOCK_ESCALATION = TABLE)
GO
