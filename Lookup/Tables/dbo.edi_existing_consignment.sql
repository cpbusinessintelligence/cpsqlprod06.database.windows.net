SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_existing_consignment] (
		[cd_id]                            [int] NOT NULL,
		[cd_company_id]                    [int] NOT NULL,
		[cd_account]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_id]                      [int] NULL,
		[cd_import_id]                     [int] NULL,
		[cd_ogm_id]                        [int] NULL,
		[cd_manifest_id]                   [int] NULL,
		[cd_connote]                       [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[cd_date]                          [smalldatetime] NULL,
		[cd_consignment_date]              [smalldatetime] NULL,
		[cd_eta_date]                      [smalldatetime] NULL,
		[cd_eta_earliest]                  [smalldatetime] NULL,
		[cd_customer_eta]                  [smalldatetime] NULL,
		[cd_pickup_addr0]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr1]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr2]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_addr3]                  [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_suburb]                 [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_postcode]               [int] NULL,
		[cd_pickup_record_no]              [int] NULL,
		[cd_pickup_confidence]             [int] NULL,
		[cd_pickup_contact]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_contact_phone]          [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr0]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr1]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr2]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_addr3]                [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_email]                [varchar](255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_suburb]               [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_postcode]             [int] NULL,
		[cd_delivery_record_no]            [int] NULL,
		[cd_delivery_confidence]           [int] NULL,
		[cd_delivery_contact]              [varchar](40) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_delivery_contact_phone]        [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_special_instructions]          [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_stats_branch]                  [int] NULL,
		[cd_stats_depot]                   [int] NULL,
		[cd_pickup_branch]                 [int] NULL,
		[cd_pickup_pay_branch]             [int] NULL,
		[cd_deliver_branch]                [int] NULL,
		[cd_deliver_pay_branch]            [int] NULL,
		[cd_special_driver]                [int] NULL,
		[cd_pickup_revenue]                [float] NULL,
		[cd_deliver_revenue]               [float] NULL,
		[cd_pickup_billing]                [float] NULL,
		[cd_deliver_billing]               [float] NULL,
		[cd_pickup_charge]                 [float] NULL,
		[cd_pickup_charge_actual]          [float] NULL,
		[cd_deliver_charge]                [float] NULL,
		[cd_deliver_payment_actual]        [float] NULL,
		[cd_pickup_payment]                [float] NULL,
		[cd_pickup_payment_actual]         [float] NULL,
		[cd_deliver_payment]               [float] NULL,
		[cd_deliver_charge_actual]         [float] NULL,
		[cd_special_payment]               [float] NULL,
		[cd_insurance_billing]             [float] NULL,
		[cd_items]                         [int] NULL,
		[cd_coupons]                       [int] NULL,
		[cd_references]                    [int] NULL,
		[cd_rating_id]                     [int] NULL,
		[cd_chargeunits]                   [int] NULL,
		[cd_deadweight]                    [float] NULL,
		[cd_dimension0]                    [float] NULL,
		[cd_dimension1]                    [float] NULL,
		[cd_dimension2]                    [float] NULL,
		[cd_volume]                        [float] NULL,
		[cd_volume_automatic]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_import_deadweight]             [float] NULL,
		[cd_import_volume]                 [float] NULL,
		[cd_measured_deadweight]           [float] NULL,
		[cd_measured_volume]               [float] NULL,
		[cd_billing_id]                    [int] NULL,
		[cd_billing_date]                  [smalldatetime] NULL,
		[cd_export_id]                     [int] NULL,
		[cd_export2_id]                    [int] NULL,
		[cd_pickup_pay_date]               [smalldatetime] NULL,
		[cd_delivery_pay_date]             [smalldatetime] NULL,
		[cd_transfer_pay_date]             [smalldatetime] NULL,
		[cd_activity_stamp]                [datetime] NULL,
		[cd_activity_driver]               [int] NULL,
		[cd_pickup_stamp]                  [datetime] NULL,
		[cd_pickup_driver]                 [int] NULL,
		[cd_pickup_pay_driver]             [int] NULL,
		[cd_pickup_count]                  [int] NULL,
		[cd_pickup_notified]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_accept_stamp]                  [datetime] NULL,
		[cd_accept_driver]                 [int] NULL,
		[cd_accept_count]                  [int] NULL,
		[cd_accept_notified]               [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_indepot_stamp]                 [datetime] NULL,
		[cd_indepot_driver]                [int] NULL,
		[cd_indepot_count]                 [int] NULL,
		[cd_transfer_notified]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_failed_stamp]                  [datetime] NULL,
		[cd_failed_driver]                 [int] NULL,
		[cd_deliver_stamp]                 [datetime] NULL,
		[cd_deliver_driver]                [int] NULL,
		[cd_deliver_pay_driver]            [int] NULL,
		[cd_deliver_count]                 [int] NULL,
		[cd_deliver_pod]                   [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_pay_notified]           [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_deliver_pay_notified]          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_printed]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_returns]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release]                       [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_release_stamp]                 [datetime] NULL,
		[cd_pricecode]                     [varchar](16) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_insurance]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_pickup_agent]                  [int] NULL,
		[cd_delivery_agent]                [int] NULL,
		[cd_agent_pod]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_desired]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_name]                [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_agent_pod_stamp]               [datetime] NULL,
		[cd_agent_pod_entry]               [datetime] NULL,
		[cd_completed]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled]                     [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_cancelled_stamp]               [datetime] NULL,
		[cd_cancelled_by]                  [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_test]                          [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_dirty]                         [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_transfer_stamp]                [datetime] NULL,
		[cd_transfer_driver]               [int] NULL,
		[cd_transfer_count]                [int] NULL,
		[cd_transfer_to]                   [int] NULL,
		[cd_toagent_notified]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_toagent_stamp]                 [datetime] NULL,
		[cd_toagent_driver]                [int] NULL,
		[cd_toagent_count]                 [int] NULL,
		[cd_toagent_name]                  [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_status]                   [varchar](64) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_notified]                 [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_info]                     [varchar](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[cd_last_driver]                   [int] NULL,
		[cd_last_stamp]                    [datetime] NULL,
		[cd_last_count]                    [int] NULL,
		[cd_accept_driver_branch]          [int] NULL,
		[cd_activity_driver_branch]        [int] NULL,
		[cd_deliver_driver_branch]         [int] NULL,
		[cd_deliver_pay_driver_branch]     [int] NULL,
		[cd_failed_driver_branch]          [int] NULL,
		[cd_indepot_driver_branch]         [int] NULL,
		[cd_last_driver_branch]            [int] NULL,
		[cd_pickup_driver_branch]          [int] NULL,
		[cd_pickup_pay_driver_branch]      [int] NULL,
		[cd_special_driver_branch]         [int] NULL,
		[cd_toagent_driver_branch]         [int] NULL,
		[cd_transfer_driver_branch]        [int] NULL
)
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_im__00AA174D]
	DEFAULT (NULL) FOR [cd_import_volume]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_me__019E3B86]
	DEFAULT (NULL) FOR [cd_measured_deadweight]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_me__02925FBF]
	DEFAULT (NULL) FOR [cd_measured_volume]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_bi__038683F8]
	DEFAULT ('0') FOR [cd_billing_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_bi__047AA831]
	DEFAULT (NULL) FOR [cd_billing_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ex__056ECC6A]
	DEFAULT ('0') FOR [cd_export_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ex__0662F0A3]
	DEFAULT ('0') FOR [cd_export2_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__075714DC]
	DEFAULT (NULL) FOR [cd_pickup_pay_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__084B3915]
	DEFAULT (NULL) FOR [cd_delivery_pay_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__093F5D4E]
	DEFAULT (NULL) FOR [cd_transfer_pay_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__0A338187]
	DEFAULT (NULL) FOR [cd_activity_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__0B27A5C0]
	DEFAULT ('0') FOR [cd_activity_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__0C1BC9F9]
	DEFAULT (NULL) FOR [cd_pickup_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__0D0FEE32]
	DEFAULT ('0') FOR [cd_pickup_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__0E04126B]
	DEFAULT ('0') FOR [cd_pickup_pay_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__0EF836A4]
	DEFAULT ('0') FOR [cd_pickup_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__0FEC5ADD]
	DEFAULT ('N') FOR [cd_pickup_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__10E07F16]
	DEFAULT (NULL) FOR [cd_accept_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__11D4A34F]
	DEFAULT ('0') FOR [cd_accept_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__12C8C788]
	DEFAULT ('0') FOR [cd_accept_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__13BCEBC1]
	DEFAULT ('N') FOR [cd_accept_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__14B10FFA]
	DEFAULT ('N') FOR [cd_indepot_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__15A53433]
	DEFAULT (NULL) FOR [cd_indepot_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__1699586C]
	DEFAULT ('0') FOR [cd_indepot_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__178D7CA5]
	DEFAULT ('0') FOR [cd_indepot_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__1881A0DE]
	DEFAULT ('N') FOR [cd_transfer_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_fa__1975C517]
	DEFAULT (NULL) FOR [cd_failed_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_fa__1A69E950]
	DEFAULT ('0') FOR [cd_failed_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__1B5E0D89]
	DEFAULT (NULL) FOR [cd_deliver_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__1C5231C2]
	DEFAULT ('0') FOR [cd_deliver_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__1D4655FB]
	DEFAULT ('0') FOR [cd_deliver_pay_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__1E3A7A34]
	DEFAULT ('0') FOR [cd_deliver_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__1F2E9E6D]
	DEFAULT ('') FOR [cd_deliver_pod]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__2022C2A6]
	DEFAULT ('N') FOR [cd_deliver_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__2116E6DF]
	DEFAULT ('N') FOR [cd_pickup_pay_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__220B0B18]
	DEFAULT ('N') FOR [cd_deliver_pay_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pr__22FF2F51]
	DEFAULT ('Y') FOR [cd_printed]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_re__23F3538A]
	DEFAULT ('N') FOR [cd_returns]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_re__24E777C3]
	DEFAULT ('N') FOR [cd_release]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_re__25DB9BFC]
	DEFAULT (NULL) FOR [cd_release_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pr__26CFC035]
	DEFAULT ('STD') FOR [cd_pricecode]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__27C3E46E]
	DEFAULT ('0') FOR [cd_insurance]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__28B808A7]
	DEFAULT ('0') FOR [cd_pickup_agent]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__29AC2CE0]
	DEFAULT ('0') FOR [cd_delivery_agent]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__2AA05119]
	DEFAULT ('N') FOR [cd_agent_pod]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__2B947552]
	DEFAULT ('N') FOR [cd_agent_pod_desired]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__2C88998B]
	DEFAULT ('') FOR [cd_agent_pod_name]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__2D7CBDC4]
	DEFAULT (NULL) FOR [cd_agent_pod_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__2E70E1FD]
	DEFAULT (NULL) FOR [cd_agent_pod_entry]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_co__2F650636]
	DEFAULT ('N') FOR [cd_completed]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ca__30592A6F]
	DEFAULT ('N') FOR [cd_cancelled]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ca__314D4EA8]
	DEFAULT (NULL) FOR [cd_cancelled_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ca__324172E1]
	DEFAULT ('') FOR [cd_cancelled_by]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_te__3335971A]
	DEFAULT ('N') FOR [cd_test]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_di__3429BB53]
	DEFAULT ('Y') FOR [cd_dirty]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__351DDF8C]
	DEFAULT (NULL) FOR [cd_transfer_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__361203C5]
	DEFAULT ('0') FOR [cd_transfer_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__370627FE]
	DEFAULT ('0') FOR [cd_transfer_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__37FA4C37]
	DEFAULT ('0') FOR [cd_transfer_to]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__38EE7070]
	DEFAULT ('N') FOR [cd_toagent_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__39E294A9]
	DEFAULT (NULL) FOR [cd_toagent_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__3AD6B8E2]
	DEFAULT ('0') FOR [cd_toagent_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__3BCADD1B]
	DEFAULT ('0') FOR [cd_toagent_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__3CBF0154]
	DEFAULT ('') FOR [cd_toagent_name]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__3DB3258D]
	DEFAULT ('') FOR [cd_last_status]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__3EA749C6]
	DEFAULT ('N') FOR [cd_last_notified]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__3F9B6DFF]
	DEFAULT (NULL) FOR [cd_last_info]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__408F9238]
	DEFAULT ('0') FOR [cd_last_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_co__40C49C62]
	DEFAULT ('0') FOR [cd_company_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__4183B671]
	DEFAULT (NULL) FOR [cd_last_stamp]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__41B8C09B]
	DEFAULT ('') FOR [cd_account]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__4277DAAA]
	DEFAULT ('0') FOR [cd_last_count]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ag__42ACE4D4]
	DEFAULT (NULL) FOR [cd_agent_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__436BFEE3]
	DEFAULT (NULL) FOR [cd_accept_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_im__43A1090D]
	DEFAULT (NULL) FOR [cd_import_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ac__4460231C]
	DEFAULT (NULL) FOR [cd_activity_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_og__44952D46]
	DEFAULT ('0') FOR [cd_ogm_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__45544755]
	DEFAULT (NULL) FOR [cd_deliver_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ma__4589517F]
	DEFAULT ('0') FOR [cd_manifest_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__46486B8E]
	DEFAULT (NULL) FOR [cd_deliver_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_co__467D75B8]
	DEFAULT ('') FOR [cd_connote]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_fa__473C8FC7]
	DEFAULT (NULL) FOR [cd_failed_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_da__477199F1]
	DEFAULT (NULL) FOR [cd_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__4830B400]
	DEFAULT (NULL) FOR [cd_indepot_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_co__4865BE2A]
	DEFAULT (NULL) FOR [cd_consignment_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_la__4924D839]
	DEFAULT (NULL) FOR [cd_last_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_et__4959E263]
	DEFAULT (NULL) FOR [cd_eta_date]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4A18FC72]
	DEFAULT (NULL) FOR [cd_pickup_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_et__4A4E069C]
	DEFAULT (NULL) FOR [cd_eta_earliest]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4B0D20AB]
	DEFAULT (NULL) FOR [cd_pickup_pay_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_cu__4B422AD5]
	DEFAULT (NULL) FOR [cd_customer_eta]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_sp__4C0144E4]
	DEFAULT (NULL) FOR [cd_special_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4C364F0E]
	DEFAULT ('') FOR [cd_pickup_addr0]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_to__4CF5691D]
	DEFAULT (NULL) FOR [cd_toagent_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4D2A7347]
	DEFAULT ('') FOR [cd_pickup_addr1]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_tr__4DE98D56]
	DEFAULT (NULL) FOR [cd_transfer_driver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4E1E9780]
	DEFAULT ('') FOR [cd_pickup_addr2]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__4F12BBB9]
	DEFAULT ('') FOR [cd_pickup_addr3]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__5006DFF2]
	DEFAULT ('') FOR [cd_pickup_suburb]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__50FB042B]
	DEFAULT ('0') FOR [cd_pickup_postcode]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__51EF2864]
	DEFAULT ('0') FOR [cd_pickup_record_no]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__52E34C9D]
	DEFAULT ('0') FOR [cd_pickup_confidence]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__53D770D6]
	DEFAULT ('') FOR [cd_pickup_contact]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__54CB950F]
	DEFAULT ('') FOR [cd_pickup_contact_phone]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__55BFB948]
	DEFAULT ('') FOR [cd_delivery_addr0]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__56B3DD81]
	DEFAULT ('') FOR [cd_delivery_addr1]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__57A801BA]
	DEFAULT ('') FOR [cd_delivery_addr2]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__589C25F3]
	DEFAULT ('') FOR [cd_delivery_addr3]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__59904A2C]
	DEFAULT ('') FOR [cd_delivery_email]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__5A846E65]
	DEFAULT ('') FOR [cd_delivery_suburb]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__5B78929E]
	DEFAULT (NULL) FOR [cd_delivery_postcode]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__5C6CB6D7]
	DEFAULT ('0') FOR [cd_delivery_record_no]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__5D60DB10]
	DEFAULT ('0') FOR [cd_delivery_confidence]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__5E54FF49]
	DEFAULT ('') FOR [cd_delivery_contact]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__5F492382]
	DEFAULT ('') FOR [cd_delivery_contact_phone]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_sp__603D47BB]
	DEFAULT (NULL) FOR [cd_special_instructions]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_st__61316BF4]
	DEFAULT (NULL) FOR [cd_stats_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_st__6225902D]
	DEFAULT (NULL) FOR [cd_stats_depot]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__6319B466]
	DEFAULT (NULL) FOR [cd_pickup_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__640DD89F]
	DEFAULT (NULL) FOR [cd_pickup_pay_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__6501FCD8]
	DEFAULT (NULL) FOR [cd_deliver_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__65F62111]
	DEFAULT (NULL) FOR [cd_deliver_pay_branch]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_sp__66EA454A]
	DEFAULT ('0') FOR [cd_special_driver]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__67DE6983]
	DEFAULT ('0') FOR [cd_pickup_revenue]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__68D28DBC]
	DEFAULT ('0') FOR [cd_deliver_revenue]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__69C6B1F5]
	DEFAULT ('0') FOR [cd_pickup_billing]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__6ABAD62E]
	DEFAULT ('0') FOR [cd_deliver_billing]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__6BAEFA67]
	DEFAULT ('0') FOR [cd_pickup_charge]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__6CA31EA0]
	DEFAULT ('0') FOR [cd_pickup_charge_actual]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__6D9742D9]
	DEFAULT ('0') FOR [cd_deliver_charge]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__6E8B6712]
	DEFAULT ('0') FOR [cd_deliver_payment_actual]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__6F7F8B4B]
	DEFAULT ('0') FOR [cd_pickup_payment]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_pi__7073AF84]
	DEFAULT ('0') FOR [cd_pickup_payment_actual]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__7167D3BD]
	DEFAULT ('0') FOR [cd_deliver_payment]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__725BF7F6]
	DEFAULT ('0') FOR [cd_deliver_charge_actual]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_sp__73501C2F]
	DEFAULT ('0') FOR [cd_special_payment]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_in__74444068]
	DEFAULT ('0') FOR [cd_insurance_billing]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_it__753864A1]
	DEFAULT (NULL) FOR [cd_items]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_co__762C88DA]
	DEFAULT (NULL) FOR [cd_coupons]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_re__7720AD13]
	DEFAULT (NULL) FOR [cd_references]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ra__7814D14C]
	DEFAULT (NULL) FOR [cd_rating_id]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_ch__7908F585]
	DEFAULT (NULL) FOR [cd_chargeunits]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_de__79FD19BE]
	DEFAULT (NULL) FOR [cd_deadweight]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_di__7AF13DF7]
	DEFAULT (NULL) FOR [cd_dimension0]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_di__7BE56230]
	DEFAULT (NULL) FOR [cd_dimension1]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_di__7CD98669]
	DEFAULT (NULL) FOR [cd_dimension2]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_vo__7DCDAAA2]
	DEFAULT (NULL) FOR [cd_volume]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_vo__7EC1CEDB]
	DEFAULT ('N') FOR [cd_volume_automatic]
GO
ALTER TABLE [dbo].[edi_existing_consignment]
	ADD
	CONSTRAINT [DF__edi_exist__cd_im__7FB5F314]
	DEFAULT (NULL) FOR [cd_import_deadweight]
GO
CREATE UNIQUE NONCLUSTERED INDEX [idx_nc_edi_existing_consignment]
	ON [dbo].[edi_existing_consignment] ([cd_id])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[edi_existing_consignment] SET (LOCK_ESCALATION = TABLE)
GO
