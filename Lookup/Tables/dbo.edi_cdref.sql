SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_cdref] (
		[cr_id]              [int] NOT NULL,
		[cr_company_id]      [int] NOT NULL,
		[cr_consignment]     [int] NOT NULL,
		[cr_reference]       [char](32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_edi_cdref]
		PRIMARY KEY
		CLUSTERED
		([cr_id])
	WITH (STATISTICS_NORECOMPUTE = ON)
)
GO
ALTER TABLE [dbo].[edi_cdref]
	ADD
	CONSTRAINT [DF__edi_cdref__cr_co__2BFE89A6]
	DEFAULT ('0') FOR [cr_company_id]
GO
ALTER TABLE [dbo].[edi_cdref]
	ADD
	CONSTRAINT [DF__edi_cdref__cr_co__2CF2ADDF]
	DEFAULT ('0') FOR [cr_consignment]
GO
ALTER TABLE [dbo].[edi_cdref]
	ADD
	CONSTRAINT [DF__edi_cdref__cr_re__2DE6D218]
	DEFAULT ('') FOR [cr_reference]
GO
CREATE NONCLUSTERED INDEX [nc_idx_edi_cdref_json]
	ON [dbo].[edi_cdref] ([cr_consignment])
	INCLUDE ([cr_reference])
	WITH ( STATISTICS_NORECOMPUTE = ON)
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[edi_cdref] SET (LOCK_ESCALATION = TABLE)
GO
