SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[edi_cdadditional_20210209_BackUp_PV_2ndSet] (
		[ca_consignment]     [int] NOT NULL,
		[ca_atl]             [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ca_dg]              [char](1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
ALTER TABLE [dbo].[edi_cdadditional_20210209_BackUp_PV_2ndSet] SET (LOCK_ESCALATION = TABLE)
GO
