SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerExceptions] (
		[Sno]                 [int] IDENTITY(1, 1) NOT NULL,
		[AccountNumber]       [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[AccountName]         [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[MailTemplateid]      [int] NULL,
		[CreatedBy]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreatedDatetime]     [datetime] NULL,
		[EditedBy]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[EditedDatetime]      [datetime] NULL,
		[isactive]            [bit] NULL
)
GO
ALTER TABLE [dbo].[CustomerExceptions] SET (LOCK_ESCALATION = TABLE)
GO
