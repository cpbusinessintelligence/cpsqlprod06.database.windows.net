SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[outgoing_consignment_event_source] (
		[SourceID]       [int] IDENTITY(1, 1) NOT NULL,
		[SourceName]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		CONSTRAINT [PK_outgoing_consignment_event_source]
		PRIMARY KEY
		CLUSTERED
		([SourceID])
)
GO
ALTER TABLE [dbo].[outgoing_consignment_event_source] SET (LOCK_ESCALATION = TABLE)
GO
