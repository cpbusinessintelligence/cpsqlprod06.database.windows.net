SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[REDIRECTION_UPDATE_WEBSITE_CONSIGNMENT_WHEN_DELIVERY_OPTION_IS_RESCHEDULE_DELIVERY]
	
AS
BEGIN

	SET NOCOUNT ON;

    UPDATE [website_tblConsignment]
	SET	
		[RescheduledDeliveryDate] = TRY_CONVERT(DATETIME, RED.[NewETA], 103),
		[SpecialInstruction] = IIF((TRY_CONVERT(DATETIME, RED.[NewETA], 103)) IS NULL, left(RED.[SpecialInstruction],35) + ';Rescheduled Delivery: ' + RED.[NewETA], left(RED.[SpecialInstruction],35)),
		[IsATl] = RED.ATL,		
		[Lookup_Updated_Stamp] = CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'),
		[Locked_For_Editing_After_Redirection] = 1
			OUTPUT
				 INSERTED.ConsignmentID
				,INSERTED.ConsignmentCode
				,INSERTED.AccountNumber
				,INSERTED.PickupFirstName
				,INSERTED.PickupLastName
				,INSERTED.PickupCompanyName
				,INSERTED.PickupEmail
				,INSERTED.PickupAddress1
				,INSERTED.PickupAddress2
				,INSERTED.PickupSuburb
				,INSERTED.PickupStateName
				,INSERTED.PickupStateID
				,INSERTED.PickupPostCode
				,INSERTED.PickupPhone
				,INSERTED.PickupCountry
				,INSERTED.NewDestinationFirstName AS DestinationFirstName
				,INSERTED.NewDestinationLastName AS DestinationLastName
				,INSERTED.NewDestinationCompanyName AS DestinationCompanyName
				,INSERTED.NewDestinationEmail AS DestinationEmail
				,INSERTED.NewDestinationAddress1 AS DestinationAddress1
				,INSERTED.NewDestinationAddress2 AS DestinationAddress2
				,INSERTED.NewDestinationSuburb AS DestinationSuburb
				,INSERTED.NewDestinationStateName AS DestinationStateName
				,INSERTED.NewDestinationStateID AS DestinationStateID
				,INSERTED.NewDestinationPostCode AS DestinationPostCode
				,INSERTED.NewDestinationPhone AS DestinationPhone
				,INSERTED.NewDestinationCountry AS DestinationCountry
				,INSERTED.TotalWeight
				,INSERTED.TotalVolume
				,INSERTED.NoOfItems
				,INSERTED.SpecialInstruction
				,INSERTED.DangerousGoods
				,INSERTED.RateCardID
				,INSERTED.CreatedDateTime
				,INSERTED.IsInternational
				,INSERTED.IsATl
				,INSERTED.InsuranceCategory
				,'UP' AS Delivery_Status_Flag
				,RED.SelectedDeliveryOption AS RedirectedDeliveryOption
				,INSERTED.RescheduledDeliveryDate
			INTO
				[redirection_website_incremental_tblConsignment]
		FROM 
			[website_tblConsignment] CON  WITH (NOLOCK)
		INNER JOIN
			[redirection_staging_tblRedirectedConsignment] RED WITH (NOLOCK)
		ON 
			CON.ConsignmentCode = RED.ConsignmentCode			
		WHERE 			
			CON.[Locked_For_Editing_After_Redirection] = 0
			AND
			RED.SelectedDeliveryOption LIKE 'Reschedule%'
			AND
			RED.ConsignmentCode LIKE ('CPW%');
END
GO
