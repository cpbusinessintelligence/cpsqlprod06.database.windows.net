SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[REDIRECTION_UPDATE_EDI_CONSIGNMENT_WHEN_DELIVERY_OPTION_IS_ATL]
	
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRANSACTION


		-- [edi_cdadditional]
		UPDATE [edi_cdadditional]
		SET
			ca_atl = IIF(RED.ATL = 0, 'N', 'Y')
		FROM 
			[edi_cdadditional] CDADD
		INNER JOIN
			[edi_consignment] CON
		ON
			CDADD.ca_consignment = CON.[cd_id]
		INNER JOIN
			[redirection_staging_tblRedirectedConsignment] RED
		ON 
			RED.ConsignmentCode = CON.cd_connote
		WHERE 
			CON.cd_locked_for_editing_after_redirection = 0				
			AND
			RED.SelectedDeliveryOption LIKE 'Authority%'
			AND
			RED.ConsignmentCode NOT LIKE ('CPW%');		
		
		-- [edi_consignment]
		UPDATE [edi_consignment]
		SET				
			[cd_pickup_state_name] = ([dbo].[EDI_GET_STATE](cd_pickup_branch)),
			[cd_delivery_state_name] = ([dbo].[EDI_GET_STATE](cd_deliver_branch)),
			--changed by AH on 24/03/2020 as it was causing issues as customers were typing large number of charcaters
			[cd_special_instructions] = left(CAST(RED.SpecialInstruction AS VARCHAR(64)), 64),
			[cd_lookup_updated_stamp] = CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'),
			[cd_locked_for_editing_after_redirection] = 1
				OUTPUT 
					INSERTED.cd_id,
					INSERTED.cd_account,
					INSERTED.cd_connote,
					INSERTED.cd_date,
					INSERTED.cd_consignment_date,
					INSERTED.cd_pickup_addr0,
					INSERTED.cd_pickup_addr1,
					INSERTED.cd_pickup_addr2,
					INSERTED.cd_pickup_addr3,
					INSERTED.cd_pickup_suburb,
					INSERTED.cd_pickup_state_name,
					INSERTED.cd_pickup_postcode,
					INSERTED.cd_pickup_contact,
					INSERTED.cd_pickup_contact_phone,
					INSERTED.cd_delivery_addr0,
					INSERTED.cd_delivery_addr1,
					INSERTED.cd_delivery_addr2,
					INSERTED.cd_delivery_addr3,
					INSERTED.cd_delivery_email,
					INSERTED.cd_delivery_suburb,
					INSERTED.cd_delivery_state_name,
					INSERTED.cd_delivery_postcode,
					'' AS [cd_delivery_country],
					INSERTED.cd_delivery_contact,
					INSERTED.cd_delivery_contact_phone,
					INSERTED.cd_special_instructions,
					INSERTED.cd_items,
					INSERTED.cd_deadweight,
					INSERTED.cd_volume,
					INSERTED.cd_pricecode,
					INSERTED.cd_insurance,
					'UP' AS cd_delivery_status_flag,
					RED.SelectedDeliveryOption AS cd_redirected_delivery_option,
					NULL AS cd_rescheduled_delivery_date
				INTO 
					[redirection_edi_incremental_consignment]
			FROM 
				[edi_consignment] CON 
			INNER JOIN
				[redirection_staging_tblRedirectedConsignment] RED
			ON 
				CON.cd_connote = RED.ConsignmentCode			
			WHERE 			
				CON.[cd_locked_for_editing_after_redirection] = 0
				AND
				RED.SelectedDeliveryOption LIKE 'Authority%'
				AND
				RED.ConsignmentCode NOT LIKE ('CPW%');
	COMMIT;
END
GO
