SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[WEBSITE_UPDATE_CONSIGNMENT]
	
AS
BEGIN

	SET NOCOUNT ON;

    UPDATE [website_tblConsignment]
	SET	
		[ConsignmentID] = INC.ConsignmentID,
		[UserID] = INC.UserID,
		[IsRegUserConsignment] = INC.IsRegUserConsignment,
		[PickupID] = INC.PickupID,
		[DestinationID] = INC.DestinationID,
		[ContactID] = INC.ContactID,
		[TotalWeight] = INC.TotalWeight,
		[TotalMeasureWeight] = INC.TotalMeasureWeight,
		[TotalVolume] = INC.TotalVolume,
		[TotalMeasureVolume] = INC.TotalMeasureVolume,
		[NoOfItems] = INC.NoOfItems,
		[SpecialInstruction] = INC.SpecialInstruction,
		[CustomerRefNo] = INC.CustomerRefNo,
		[ConsignmentPreferPickupDate] = INC.ConsignmentPreferPickupDate,
		[ConsignmentPreferPickupTime] = INC.ConsignmentPreferPickupTime,
		[ClosingTime] = INC.ClosingTime,
		[DangerousGoods] = INC.DangerousGoods,
		[Terms] = INC.Terms,
		[RateCardID] = INC.RateCardID,
		[LastActivity] = INC.LastActivity,
		[LastActiivityDateTime] = INC.LastActiivityDateTime,
		[ConsignmentStatus] = INC.ConsignmentStatus,
		[EDIDataProcessed] = INC.EDIDataProcessed,
		[ProntoDataExtracted] = INC.ProntoDataExtracted,
		[IsBilling] = INC.IsBilling,
		[IsManifested] = INC.IsManifested,
		[CreatedDateTime] = INC.CreatedDateTime,
		[CreatedBy] = INC.CreatedBy,
		[UpdatedDateTTime] = INC.UpdatedDateTTime,
		[UpdatedBy] = INC.UpdatedBy,
		[IsInternational] = INC.IsInternational,
		[IsDocument] = INC.IsDocument,
		[IsSignatureReq] = INC.IsSignatureReq,
		[IfUndelivered] = INC.IfUndelivered,
		[ReasonForExport] = INC.ReasonForExport,
		[TypeOfExport] = INC.TypeOfExport,
		[Currency] = INC.Currency,
		[IsInsurance] = INC.IsInsurance,
		[IsIdentity] = INC.IsIdentity,
		[IdentityType] = INC.IdentityType,
		[IdentityNo] = INC.IdentityNo,
		[Country-ServiceArea-FacilityCode] = INC.[Country-ServiceArea-FacilityCode],
		[InternalServiceCode] = INC.InternalServiceCode,
		[NetSubTotal] = INC.NetSubTotal,
		[IsATl] = INC.IsATl,
		[IsReturnToSender] = INC.IsReturnToSender,
		[HasReadInsuranceTc] = INC.HasReadInsuranceTc,
		[NatureOfGoods] = INC.NatureOfGoods,
		[OriginServiceAreaCode] = INC.OriginServiceAreaCode,
		[ProductShortName] = INC.ProductShortName,
		[SortCode] = INC.SortCode,
		[ETA] = INC.ETA,
		[CTIManifestExtracted] = INC.CTIManifestExtracted,
		[isprocessed] = INC.isprocessed,
		[IsAccountCustomer] = INC.IsAccountCustomer,
		[InsuranceAmount] = INC.InsuranceAmount,
		[CourierPickupDate] = INC.CourierPickupDate,
		[CalculatedTotal] = INC.CalculatedTotal,
		[CalculatedGST] = INC.CalculatedGST,
		[ClientCode] = INC.ClientCode,
		[isCreateEzy2ShipShipment] = INC.isCreateEzy2ShipShipment,
		[isShipmentSent2NZPost] = INC.isShipmentSent2NZPost,
		[PromotionCode] = INC.PromotionCode,
		[IsHubApiProcessed] = INC.IsHubApiProcessed,
		[InsuranceCategory] = INC.InsuranceCategory,
		[AccountNumber] = INC.AccountNumber,
		[PickupFirstName] = INC.PickupFirstName,
		[PickupLastName] = INC.PickupLastName,
		[PickupCompanyName] = INC.PickupCompanyName,
		[PickupEmail] = INC.PickupEmail,
		[PickupAddress1] = INC.PickupAddress1,
		[PickupAddress2] = INC.PickupAddress2,
		[PickupSuburb] = INC.PickupSuburb,
		[PickupStateName] = INC.PickupStateName,
		[PickupStateID] = INC.PickupStateID,
		[PickupPostCode] = INC.PickupPostCode,
		[PickupPhone] = INC.PickupPhone,
		[PickupCountry] = INC.PickupCountry,
		[DestinationFirstName] = INC.DestinationFirstName,
		[DestinationLastName] = INC.DestinationLastName,
		[DestinationCompanyName] = INC.DestinationCompanyName,
		[DestinationEmail] = INC.DestinationEmail,
		[DestinationAddress1] = INC.DestinationAddress1,
		[DestinationAddress2] = INC.DestinationAddress2,
		[DestinationSuburb] = INC.DestinationSuburb,
		[DestinationStateName] = INC.DestinationStateName,
		[DestinationStateID] = INC.DestinationStateID,
		[DestinationPostCode] = INC.DestinationPostCode,
		[DestinationPhone] = INC.DestinationPhone,
		[DestinationCountry] = INC.DestinationCountry,
		[Lookup_Updated_Stamp] = CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time')
		FROM 
			[website_tblConsignment] CON  WITH (NOLOCK)
		INNER JOIN
			[website_incremental_tblConsignment] INC  WITH (NOLOCK)
		ON
			CON.ConsignmentCode = INC.ConsignmentCode
			WHERE 
			INC.Delivery_Status_Flag = 'UP'
			AND
			CON.[Locked_For_Editing_After_Redirection] = 0 
			AND 
			CON.[Locked_For_Editing_After_Redelivery] = 0

END
GO
