SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO






CREATE PROCEDURE [dbo].[EDI_GET_CONSIGNMENT_JSON_bkup_AH_Labelless_20210303] 
(
	@Lower INT,
	@Upper INT
)
/*
	
	02/09/2020 - AH - Change made for Onboard notification to include fields like ReceiverEmail, DisableTracking Flag and PriorityExtRef
	15/09/2020 - AH - Added more customers to receive onboard notifications
	25/09/2020 - AH - Changes flag for customers to receive Onboard notifications not in the Customer Exceptions List
*/
AS
BEGIN

	SET NOCOUNT ON;

	SELECT 
		ConsignmentNumber, 		
		JSONString,
		SRC.[SourceID]
		FROM
		(SELECT 
			[id], 
			cd_connote AS ConsignmentNumber, 			
			(SELECT 
					CONVERT(VARCHAR, CON.cd_connote) AS CONSIGNMENTNUMBER,
					CONVERT(DATE, CON.[cd_consignment_date]) AS CONSIGNMENTDATE,
					CONVERT(DATE, CON.[cd_date]) AS CREATEDDATE,	
					ISNULL(CON.cd_pricecode, '') AS SERVICECODE,
					ISNULL(CON.[cd_account], '') AS ACCOUNTNUMBER,
					'' AS SENDERCOMPANYNAME,
					ISNULL(CON.[cd_pickup_addr0], '') AS SENDERNAME,
					ISNULL(CON.[cd_pickup_addr1], '') AS SENDERADDRESS1,
					ISNULL(CON.[cd_pickup_addr2], '') AS SENDERADDRESS2,
					ISNULL(CON.[cd_pickup_addr3], '') AS SENDERADDRESS3,
					ISNULL(CON.[cd_pickup_suburb], '') AS SENDERLOCATION,					
					[dbo].[EDI_GET_STATE](CON.cd_pickup_branch) AS SENDERSTATE,
					CONVERT(VARCHAR, CON.[cd_pickup_postcode]) AS SENDERPOSTCODE,
					'' AS SENDERCOUNTRY,
					--'' AS RECEIVERCOMPANYNAME,
					ISNULL(CON.[cd_delivery_addr0], '') AS RECEIVERCOMPANYNAME,
					ISNULL(CON.[cd_delivery_addr0], '') AS RECEIVERNAME,
					ISNULL(CON.[cd_delivery_addr1], '') AS RECEIVERADDRESS1,
					ISNULL(CON.[cd_delivery_addr2], '') AS RECEIVERADDRESS2,
					ISNULL(CON.[cd_delivery_addr3], '') AS RECEIVERADDRESS3,
					ISNULL(CON.[cd_delivery_suburb], '') AS RECEIVERLOCATION,				
					[dbo].[EDI_GET_STATE](CON.cd_deliver_branch) AS RECEIVERSTATE,
					CONVERT(VARCHAR, CON.[cd_delivery_postcode]) AS RECEIVERPOSTCODE,
					'' AS RECEIVERCOUNTRY,
					ISNULL(CON.[cd_delivery_contact], '') AS RECEIVERCONTACT,
					--change for Onboard Notifications
					ISNULL(CON.[cd_delivery_email], '') AS RECEIVEREMAIL,
					ISNULL(CON.[cd_delivery_contact_phone], '') AS RECEIVERPHONE,
					ISNULL(CON.[cd_special_instructions], '') AS SPECIALINSTRUCTIONS,
					CONVERT(VARCHAR, CON.[cd_items]) AS TOTALLOGISTICSUNITS,
					CONVERT(VARCHAR, CON.[cd_deadweight]) AS TOTALDEADWEIGHT,
					CONVERT(VARCHAR, CON.[cd_volume]) AS TOTALVOLUME,
					[dbo].[EDI_GET_INSURANCE_CATEGORY](CON.[cd_insurance]) AS INSURANCECATEGORY,
					IIF(CDADD.ca_dg IS NULL OR TRIM(CDADD.ca_dg) = '', 'N', TRIM(CDADD.ca_dg)) AS DANGEROUSGOODSFLAG,
					ISNULL(CON.[cd_pickup_contact], '') AS PICKUPCONTACT,
					ISNULL(CON.[cd_pickup_contact_phone], '') AS PICKUPPHONE,
					IIF(CDADD.ca_atl IS NULL OR TRIM(CDADD.ca_atl) = '', 'N', TRIM(CDADD.ca_atl)) AS ATLFLAG,
					[dbo].[GET_FREIGHTCATEGORY](CON.cd_pricecode) AS FREIGHTCATEGORY,
					'' AS ALERT,
					ISNULL(CON.[cd_delivery_status_flag], '') AS DELIVERYSTATUSFLAG,
					'Normal' AS DELIVERYTYPE,
					'' AS RESCHEDULEDDELIVERYDATE,
					'N' AS ISINTL,
					--change for Onboard Notifications
					CASE WHEN dbo.EDI_GET_CUSTOMER_EXCEPTIONS(CON.[cd_account]) = 'true' THEN 'FALSE' ELSE 'TRUE' END AS DISABLETRACKING,
					NULL AS PRIORITYEXTREF,
					(SELECT 
						TRIM(COUP.[cc_coupon]) AS LABELNUMBER,
						TRIM(COUP.[cc_unit_type]) AS UNITTYPE,
						'TO BE DECIDED' AS INTERNALLABELNUMBER
						FROM [edi_cdcoupon] COUP WITH (NOLOCK) 
						WHERE CON.[cd_id] = COUP.cc_consignment FOR JSON PATH, INCLUDE_NULL_VALUES) AS 'LABELS',
					(SELECT 
						TRIM(REF.cr_reference) AS ADDITIONALREFERENCE
						FROM [edi_cdref] REF WITH (NOLOCK) 
						WHERE CON.cd_id = REF.cr_consignment
						FOR JSON PATH, INCLUDE_NULL_VALUES) AS 'REFERENCES'
				FROM [edi_incremental_consignment] CON WITH (NOLOCK) 
				LEFT JOIN [edi_cdadditional] CDADD WITH (NOLOCK) 
					ON CON.[cd_id] = CDADD.ca_consignment
					WHERE CON.[cd_id] = RelationalJSONData.cd_id
				FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
			) JSONString
		FROM [edi_incremental_consignment] AS RelationalJSONData  WITH (NOLOCK) 
		WHERE  [Id] BETWEEN @Lower AND @Upper
		) AS JSONOnly
		LEFT JOIN [dbo].[outgoing_consignment_event_source] SRC WITH (NOLOCK) 
		ON SRC.[SourceName] = 'EDI'
END
GO
